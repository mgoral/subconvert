Legal Note
==========

Subconvert logo:
    copyright by Michał Góral and released under GNU GPL3 license (part of
    Subconvert, the same license).

Feather Icons:
    copyright by Cole Bemis and released under MIT license (LICENSE-feather).
