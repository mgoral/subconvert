from unittest.mock import Mock, call
import pytest

from subconvert.utils.cleanup import Cleanup


@pytest.fixture
def manager():
    manager = Mock()
    manager.attach_mock(Mock(), 'first')
    manager.attach_mock(Mock(), 'second')
    manager.attach_mock(Mock(), 'third')
    return manager


def test_cleanup_order(manager):
    clean = Cleanup()
    clean.register(manager.first)
    clean.register(manager.second)
    clean.register(manager.third)
    clean.start()
    assert manager.mock_calls == [call.third(), call.second(), call.first()]


def test_many_cleanups(manager):
    clean = Cleanup()
    clean.register(manager.first)
    clean.start()
    assert manager.mock_calls == [call.first()]

    manager.reset_mock()
    clean.register(manager.third)
    clean.register(manager.second)
    clean.register(manager.first)
    clean.start()
    assert manager.mock_calls == [call.first(), call.second(), call.third()]


def test_same_functions(manager):
    clean = Cleanup()
    clean.register(manager.first)
    clean.register(manager.first)
    clean.register(manager.first)
    clean.start()
    assert manager.mock_calls == [call.first(), call.first(), call.first()]


def test_exceptions(manager):
    manager.second.side_effect = Exception('Boom!')

    clean = Cleanup()
    clean.register(manager.first)
    clean.register(manager.second)
    clean.register(manager.third)

    with pytest.raises(Exception) as excinfo:
        clean.start()

    assert isinstance(excinfo.value, Exception)
    assert 'Boom!' == str(excinfo.value)
    assert manager.mock_calls == [call.third(), call.second(), call.first()]
