# Copyright (C) 2017 Michal Goral.
# 
# This file is part of Subconvert
# 
# Subconvert is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Subconvert is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Subconvert. If not, see <http://www.gnu.org/licenses/>.

import pytest

from subconvert.gui.SubtitleCommands import NewSubtitles
from subconvert.utils.PropertyFile import (SubtitleProperties,
        PropertiesFileApplier)

def test_setters_getters(subparser):
    pf = SubtitleProperties(subparser.formats)

    # check defaults
    assert pf.autoInputEncoding is True
    assert pf.autoFps is True
    assert pf.fps == 23.976
    assert pf.inputEncoding is None
    assert pf.outputEncoding is None
    assert pf.changeEncoding is False
    assert pf.changeFormat  is False
    assert pf.outputFormat not in subparser.formats

    # check setters and getters
    pf.autoInputEncoding = False
    assert pf.autoInputEncoding is False

    pf.autoFps = False
    assert pf.autoFps is False

    pf.fps = 30
    assert pf.fps == 30

    pf.inputEncoding = 'utf-8'
    assert pf.inputEncoding == 'utf-8'

    pf.outputEncoding = 'utf-8'
    assert pf.outputEncoding == 'utf-8'

    pf.changeEncoding = True
    assert pf.changeEncoding is True

    pf.changeFormat = True
    assert pf.changeFormat is True

    for fmt in subparser.formats:
        pf.outputFormat = fmt
        assert pf.outputFormat is fmt

    with pytest.raises(TypeError):
        pf.outputFormat = 123


def test_save_load(subparser, tmpdir):
    f = tmpdir.mkdir("spf").join("spf")
    pf1 = SubtitleProperties(subparser.formats)

    pf1.autoFps = False
    pf1.fps = 30
    pf1.autoInputEncoding = False
    pf1.inputEncoding = 'utf-8'
    pf1.outputEncoding = 'utf-8'
    pf1.changeEncoding = True
    pf1.changeFormat = True
    pf1.outputFormat = list(subparser.formats)[0]

    pf1.save(str(f))

    pf2 = SubtitleProperties(subparser.formats)
    pf2.load(str(f))

    assert pf2.fps == pf1.fps
    assert pf2.inputEncoding == pf1.inputEncoding
    assert pf2.outputEncoding == pf1.outputEncoding
    assert pf2.outputFormat == pf1.outputFormat
    assert pf2.autoFps == pf1.autoFps
    assert pf2.autoInputEncoding == pf1.autoInputEncoding
    assert pf2.changeFormat == pf1.changeFormat


def test_applier(subparser, gensubs, subdata):
    subs = gensubs(20)
    data = subdata.createDataFromFile(subs)

    pf = SubtitleProperties(subparser.formats)
    pf.autoFps = False
    pf.fps = 30
    pf.autoInputEncoding = False
    pf.inputEncoding = 'utf-8'
    pf.outputEncoding = 'utf-8'
    pf.changeEncoding = True
    pf.changeFormat = True
    pf.outputFormat = list(subparser.formats)[0]

    applier = PropertiesFileApplier(pf)
    applier.applyFor(subs, data)

    assert data.fps == pf.fps
    assert data.inputEncoding == pf.inputEncoding
    assert data.outputEncoding == pf.outputEncoding
    assert data.outputFormat == pf.outputFormat
