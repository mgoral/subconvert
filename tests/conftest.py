# Copyright (C) 2016 Michal Goral.
#
# This file is part of Subconvert
#
# Subconvert is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Subconvert is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Subconvert. If not, see <http://www.gnu.org/licenses/>.

import os
import glob
import collections
import json
import locale
import pytest
import subprocess

from fixtures.autouse import *
from fixtures.gui import *

from subconvert.utils.SubFile import File
from subconvert.parsing.FrameTime import FrameTime
from subconvert.parsing.Core import Subtitle, SubManager, SubParser
import subconvert.parsing.Formats as _Formats
from subconvert.utils.SubtitleData import SubtitleData

@pytest.fixture
def args(monkeypatch):
    def _f(a):
        try:
            args = a.split()  # accept string
        except AttributeError:
            args = a          # accept list

        if len(args) == 0 or args[0] != 'subconvert':
            args.insert(0, 'subconvert')

        monkeypatch.setattr('sys.argv', args)
    return _f


@pytest.fixture
def subparser():
    p = SubParser()
    p.registerFormat(_Formats.MPL2)
    p.registerFormat(_Formats.MicroDVD)
    p.registerFormat(_Formats.SubRip)
    p.registerFormat(_Formats.SubViewer)
    p.registerFormat(_Formats.TMP)
    return p


def _subtitles(subs_dir_name):
    curr_dir = os.path.dirname(os.path.realpath(__file__))
    return glob.glob(os.path.join(curr_dir, subs_dir_name, '*.txt'))

@pytest.fixture
def sublist():
    def _f(subs_dir_name):
        return _subtitles(subs_dir_name)
    return _f


# We need similar fixtures for subtitles stored in different directories so
# we'll just generate these fixtures
def make_subtitle_fixture(subs_dir_name):
    @pytest.fixture(params=_subtitles(subs_dir_name))
    def _fixture(request):
        def _json_decoder(dct):
            if '__FrameTime__' in dct:
                fps = dct.get('fps', 25)
                initializers = {'time': FrameTime.InitTimeStr,
                                'frames': FrameTime.InitFrames,
                                'ms': FrameTime}
                for arg in initializers.keys():
                    val = dct.get(arg)
                    if val is not None:
                        return initializers[arg](val, fps)
            elif '__Subtitle__' in dct:
                return Subtitle(dct.get('start'), dct.get('end'), dct.get('text'))
            return dct

        _SubParams = collections.namedtuple('SubParams', ['file', 'description'])

        sub_name = request.param
        descr_name = '%s.json' % os.path.splitext(sub_name)[0]
        with open(descr_name) as f:
            descr = json.load(f, object_hook=_json_decoder)
            return _SubParams(file=File(sub_name), description=descr)
    return _fixture

subtitle = make_subtitle_fixture('subs')
subtitle_nook = make_subtitle_fixture('subs_nook')

@pytest.fixture
def gensubs(tmpdir):
    def _f(no):
        f = tmpdir.mkdir("gensubs").join("generated.txt")

        # 0:  0, 10
        # 1: 20, 30
        # 2: 40, 50
        # 3: 60, 70
        # 4: 80, 90
        subs = ['{%d}{%d}Subtitle %d' % (20 * i, 20 * i + 10 , i)
                for i in range(no)]
        f.write(os.linesep.join(subs))
        return str(f)
    return _f

@pytest.fixture
def genvid(tmpdir):
    def _f(*, time=10, fps=23.976):
        d = tmpdir.mkdir('genvid')
        fname = os.path.join(str(d), 'vid-%ss.mp4' % time)

        # generate h264 video in mp4 container
        subprocess.call(['ffmpeg', '-t', str(time), '-r', str(fps),
            '-f', 'lavfi', '-i', 'color=c=black:s=qcif', '-c:v', 'libx264',
            '-tune', 'stillimage', '-pix_fmt', 'yuv420p', fname])

        return fname
    return _f
