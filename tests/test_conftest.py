# Copyright (C) 2017 Michal Goral.
# 
# This file is part of Subconvert
# 
# Subconvert is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Subconvert is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Subconvert. If not, see <http://www.gnu.org/licenses/>.

"""These are meta tests of test environment. Please reconsider your life
if these fail or if you want to make them less strict, as a lot of tests might
require some of properties which are tested here"""

import os
import pytest

from subconvert.utils.SubFile import File

subdirs = ['subs', 'subs_nook']


@pytest.mark.meta
@pytest.mark.parametrize('subdir', subdirs)
def test_number_of_sub_files(sublist, subdir):
    '''Require the minimum number of physical subtitles. It's required e.g. when
    testing file lists.'''
    assert len(sublist(subdir)) > 3
    assert len(sublist('subs_nook')) > 3


@pytest.mark.meta
@pytest.mark.parametrize('subdir', subdirs)
def test_number_subtitles(sublist, subparser, subdir):
    '''Require a minimum number of subtitles in each subtitle file'''
    for sub in sublist(subdir):
        assert len(subparser.parse(File(sub).read())) > 1


@pytest.mark.meta
def test_environ():
    '''Check that test-specific environment variables are automatically set.'''
    xdg_config_home = os.environ.get('XDG_CONFIG_HOME')
    assert xdg_config_home, 'XDG_CONFIG_HOME not set'
    assert not xdg_config_home.startswith(os.path.expanduser('~')), \
           'XDG_CONFIG_HOME inside user directory, should be in /tmp.'


@pytest.mark.meta
def test_subparser(subparser):
    '''Check that default subparser fixture has registered sufficient number of
    formats.'''
    assert len(subparser.formats) > 4
