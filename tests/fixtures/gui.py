# Copyright (C) 2017 Michal Goral.
# 
# This file is part of Subconvert
# 
# Subconvert is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Subconvert is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Subconvert. If not, see <http://www.gnu.org/licenses/>.

import pytest

from PyQt5.QtWidgets import QMessageBox

from subconvert.gui.DataModel import DataController

@pytest.fixture
def subdata(subparser):
    return DataController(subparser)


@pytest.fixture(autouse=True)
def discard_changes(monkeypatch):
    def _discard(*args, **kwargs):
        return QMessageBox.Discard, []
    monkeypatch.setattr('subconvert.gui.MainWindow.ask_about_changes', _discard)
    monkeypatch.setattr('subconvert.gui.SubtitleTabs.ask_about_changes', _discard)
