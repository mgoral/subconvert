[[installation]]
== Installation

=== Install from PYPI

----
$ pip3 install --user subconvert
----

=== Install with tox

If you cloned a git repository, you can install Subconvert with help of tox.

WARNING: If your system has Python version lower than 3.5, you'll need to
manually install PyQt as it's not available via PYPI.

----
$ cd subconvert
$ tox -e venv
$ ln -s {.venv,$HOME/.local}/bin/subconvert
$ ln -s {.venv,$HOME/.local}/share/applications/subconvert.desktop
----


=== Install with setup.py

WARNING: these methods are not recommended for ordinary users as they don't
manage some dependencies automatically. Installation methods from PYPI or with
tox are preferable.

You can alternatively create a Python distribution (like bdist_wheel) and
install it:

----
$ cd subconvert
$ python3 setup.py bdist_wheel
$ pip3 install dist/\*.whl
----

Or install it directly:

----
$ cd subconvert
$ python3 setup.py install
----


=== Removing

If you installed Subconvert with pip, uninstalling it is simply calling
uninstall:

----
$ pip3 uninstall subconvert
----

Otherwise you'll have to manually remove all subconvert files, i.e.:

* `$prefix/lib/python*/site-packages/subconvert`
* `$prefix/bin/subconvert`
* `$prefix/share/applications/subconvert.desktop`
* `$prefix/share/icons/hicolor/*/apps/aubconvert.{svg,png}`

=== Dependencies

NOTE: The following list includes all Subconvert dependencies. Some of themm
will be installed automatically will be installed automatically by used tools
(pip and setuptools), but the list might come handy if you're e.g. creating
Subconvert package.

NOTE: Build dependencies are only needed when you intend to create Python dist
packages manually.

[.center, width=80%, cols="^3m,^5", options=header]
.Summary of Subconvert dependencies
|===
| Package               | Comment

2+^.^a| *Runtime dependencies*

| python3.5+            |
| python3-pyqt5         | installed automatically by pip
| python3-chardet       | installed automatically by pip
| python3-pymediainfo   | installed automatically by pip
| libmediainfo          | optional, pymediainfo won't work without it
| python-mpv            | optional, installed automatically by pip, needed for
                          video playback
| libmpv                | optional, needed for video playback

2+^.^a| *Build dependencies*

| setuptools            |
| setuptools_scm        | installed automatically by setuptools
| babel                 | installed automatically by setuptools
| pyrcc5                | usually comes with `pyqt5-dev-tools`

2+^.^a| *Documentation dependencies*

| asciidoctor           | if missing, documentation won't be included in
|===
